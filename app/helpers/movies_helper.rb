module MoviesHelper
    def movie_categorie_label(categories)
        raw categories.map { |cat| "<span>#{cat.name}</span>"}.join(', ')
    end
end
