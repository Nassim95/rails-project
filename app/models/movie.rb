class Movie < ApplicationRecord
    belongs_to :user
    has_and_belongs_to_many :categories
    belongs_to :location
end
