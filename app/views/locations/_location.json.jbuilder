json.extract! location, :id, :status, :created_at, :updated_at
json.url location_url(location, format: :json)
