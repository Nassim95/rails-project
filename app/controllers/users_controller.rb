class UsersController < ApplicationController
    before_action :set_user, only: [:show, :edit, :update, :destroy]
    before_action :authenticate_user!
    
    def index
        @users = User.all
    end
    
    def show
        @movies = @user.movies
    end
    
    def edit
    end
    
    def destroy
        @user.destroy
    end
    
    private
    def set_user
        @user = User.find(params[:id])
    end
    
    def users_params
        params.require(:user).permit(:email, :password)
    end
    
end
