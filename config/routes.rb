Rails.application.routes.draw do
  resources :locations
  resources :categories
  devise_for :users
  resources :users
  resources :movies
  
  root to: 'home#index'
end
