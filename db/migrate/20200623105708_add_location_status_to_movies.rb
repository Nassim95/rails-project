class AddLocationStatusToMovies < ActiveRecord::Migration[6.0]
  def change
    add_column :movies, :location_status, :string
  end
end
